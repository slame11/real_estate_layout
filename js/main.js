var Main = {

    banner_interval : {},
    interval_time : 8000,
    initLangSelect: function () {
        $('select:not(.form-control)').ddslick();
    },

    setBannerInterval: function(){
        var self = this;
        clearInterval(this.banner_interval);
        this.banner_interval = setInterval(function () {
            var scrolltop = $('body').scrollTop();
            if( $('.navbar-toggler').hasClass('collapsed')){
                if($('.banner-content__item.active').is(':last-child')){
                    self.chooseBannerItem($('.banner-content__item:first-child')[0]);
                }else{
                    self.chooseBannerItem($('.banner-content__item.active').next()[0]);
                }
            }
        },self.interval_time)
    },


    chooseBannerItem: function(self){
        var Main = this;
        if($(self).hasClass('active')){
            return ;
        }
        var item = $(self).data('item');
        $('.banner-backgrounds__items').find('>div').removeClass('active');
        $('.banner-backgrounds__items').find('[data-item='+item+']').addClass('active');
        Main.setBannerInterval();
        var big_descr = $('.banner-content__item.active').find('.banner-content__item-text__description-big');
        big_descr.hide();
        setTimeout(function () {
            big_descr.show();
        },800)
        $('.banner-content__item').removeClass('active');
        $(self).addClass('active');
    },

    initBanner: function () {

        var self= this;
        var start_item = $('.banner-content__item.active').data('item');
        $('.banner-backgrounds__items').find('[data-item='+start_item+']').addClass('active');
          $('.banner-content__item').on('click',function () {
                self.chooseBannerItem(this);
          })

        document.addEventListener('scroll', function (event) {
                if($('body').scrollTop() > 120){
                    clearInterval(self.banner_interval);
                }else{
                    self.setBannerInterval();
                }
        }, true /*Capture event*/);
        this.setBannerInterval();

    },
    initOwl: function(){

        var owl = $('.section_happy_clients_slider_wrapper_carousel');
        owl.owlCarousel({
            loop:true,
            margin:60,
            responsive:{
                0:{
                    items:1
                },
                768:{
                    items:2
                },
                1000:{
                    items:3
                }
            }
        });

        owl = $('#home_product_carousel');

        owl.owlCarousel({
            loop:true,
            margin:20,
            responsive:{
                0:{
                    items:1
                },
                768:{
                    items:2
                },
                1000:{
                    items:3
                }
            }
        });
    },
    mainFilter: function () {
        //datepicker
        $('#date_filter').datepicker({
            uiLibrary: 'bootstrap4',
            icons :{
                rightIcon: '<img style="display:inline-block;" src="images/calendar.png" />'
            },
        });

        //plus/minus
        $('.home_catalog-filter').on('click','.btn_plus',function () {
            var input = $(this).parent().find('input');
            input.val(parseInt(input.val()) + 1);
        }).on('click','.btn_minus',function () {
            var input = $(this).parent().find('input');
            var new_val = parseInt(input.val()) - 1;
            input.val(new_val > 0 ? new_val : 1);
        });

        //sliders
        $.each($( ".filter-slider" ),function (key, val) {
            noUiSlider.create(val, {
                start: [parseInt($(val).data('selected-min')),parseInt($(val).data('selected-max'))],
                connect: true,
                range: {
                    'min': parseInt($(val).data('min')),
                    'max': parseInt($(val).data('max'))
                }
            });

            val.noUiSlider.on('update', function () {
                var values_event = val.noUiSlider.get();
                $(val).parent().find('.home_catalog-filter__advanced-slider__min').text(parseInt(values_event[0]));
                $(val).parent().find('.home_catalog-filter__advanced-slider__max').text(parseInt(values_event[1]));
            });
            var values = val.noUiSlider.get();
            $(val).parent().find('.home_catalog-filter__advanced-slider__min').text(parseInt(values[0]));
            $(val).parent().find('.home_catalog-filter__advanced-slider__max').text(parseInt(values[1]));
        });
        
        //Open/Close advanced search
        $('#advanced_search').on('click', function () {
            $('.home_catalog-filter').toggleClass('opened');
        })
    },
    contactForm: function () {
        $('#send_contact_form').on('click',function () {
            $('[data-remodal-id=contacts_block_modal]').remodal({});
        })
    },
    initClickMore: function () {
        $('.product_card_asside_agents_item_more').on('click',function () {
            $(this).closest('.product_card_asside_agents_item').find('.product_card_asside_agents_item_phones').show();
            $(this).hide();
        })
    },
    changeProductView: function () {
        $('[data-change-product-view]').on('click',function () {
            var type = $(this).data('change-product-view');
            $(this).parent().find('.active').removeClass('active');
            $(this).addClass('active');
            $('.catalog_page-content__products-products').attr('data-view',type)
        });
    },
    init: function () {
        this.initLangSelect();
        this.initBanner();
        this.initOwl();
        this.mainFilter();
        this.contactForm();
        this.initClickMore();
        this.changeProductView();
    }
};

$(document).ready(function () {
    Main.init();
})